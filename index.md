---
layout: default
title: Dockerized Twitter Clone Project
---

GitLab handles of contributors:  
[@BeataJoKo](https://gitlab.com/BeataJoKo)  
[@czuforb](https://gitlab.com/czuforb)  
[@kreve](https://gitlab.com/kreve)  
[@TobiasJZagal](https://gitlab.com/TobiasJZagal)

## Brief Description of Our App  
The app is a hard coded simplified Twitter clone. We utilized HTML, CSS, JavaScript, PHP and MySQL. Lately we concentrated mostly on setting up the development environment with sufficient Version Control and Software Quality Assurance in a CI/CD Pipeline. Want to join the project? Then read on 🦄  

## Running the App
Application can be built and run through use of a single commands. We separated the service into multiple profiles to differentiate between the development and production environment. Using this method, the Adminer service is only running on development - or debug - mode when it’s necessary. This is a security measure, as exposing yet another service on a port could be a potential leak.  
Running ``` $ docker-compose up ``` builds and runs the production ready website, exposing the website on the default HTTP port 80.
Running  ``` $ docker-compose --profiles development up ``` builds and runs the development environment also exposing the Adminer interface on port 8080.

## Code Standards  
#### HTML
For our HTML code our coding conventions is to keep it organized by separating the key elements, such as separating page main columns and home subpages through comment line. For elements that are within a container such as a div tag or similar, we made an indentation of one tab space so it would look as following:

```
<div onclick="openLogout()">
            <img src="<?=$_SESSION['profileIMG']?>">
            <div>
                <p><strong><?=$_SESSION['name']?></strong></p>
                <p>@<?=$_SESSION['name']?></p>
            </div>
            <svg viewBox="0 0 24 24"><path d="M20.207 8.147c-.39-.39-1.023-.39-1.414 0L12 14.94 5.207 8.147c-.39-.39-1.023-.39-1.414 0-.39.39-.39 1.023 0 1.414l7.5 7.5c.195.196.45.294.707.294s.512-.098.707-.293l7.5-7.5c.39-.39.39-1.022 0-1.413z"></path></svg>
        </div>
    </nav>
<!-- end left column-->

    <main>
        <section id="home">
        <header>
```
#### JavaScript
For our JavaScript coding conventions we use a lot of the same principles as in our HTML coding conventions. We keep each eventlistener and function definition separated with one line and additional comment line for bigger blocks. All code within a function or an if statement is intended with one tab space in the code. In this example it is also shown how we open and close brackets, with the opening bracket being on the same line as the function and the closing bracket being on its own line right after the last piece of code in that function.

```
// Logout Open and Close
function openLogout(){
    var logoutBox = document.getElementById("LogoutModal");
        logoutBox.style.display = "grid";

    window.onclick = function(event) {
        if (event.target == logoutBox) {
            logoutBox.style.display = "none";
        }
    }
}
```
#### PHP
Corresponding code convention is applied to apis’ php file: one comment line before block or description of function, one tab space after if statement or function definition, first curly bracket opening in line of statement, closing curly bracket on own line:

```
//session engine
session_start();
if( ! isset($_SESSION['name']) ){
  header('Location: /../login.php');
}

try{
    $sTweetId = uniqid();

    if( ! isset($_POST['tweetText']) ){
      http_response_code(400);
      header('Content-Type: application/json');
      echo '{"error":"missing text"}';
      exit();
    }
```
#### CSS
Additionally in CSS styling files code convention: after the comment line, one line space separates the block of code that layout correspondent separate block in HTML. After each declaration of element one line break is applied for readability. Every line of element description is intended with one tab space.

```
/* ###########################################*/

nav{
    grid-column: 1;
    display: grid;
    grid-template-rows: repeat(11, 1fr);
    padding: 1rem 0px;
    font-size: 1.4rem;
    position: fixed;
}
```



## Version Control System  
#### Git Log  
Every commit should have a relevant description for easy identification of implementation. Message of the commit should include a flag of new future implementation (ADD) or current code debugging (FIX). In consequence, git log history could be straightforwardly filtered not only through team members and history timeline but also a commit flag inside a commit message. Which will also simplify a process of rolling back to the previous version if the error will occur or find a code which the developer would like to reuse.  

#### Git Branches Plan 
For each feature (regardless ADD or FIX flag) a new branch should be created with a relevant descriptive name. Additional message should be provided. The pull request should be monitored by a supervisor through Approve or Disapprove of merge request. Thus, supervisor review will also be taken into consideration of continuous delivery, improving the speed of manually updating of the project with small features.








