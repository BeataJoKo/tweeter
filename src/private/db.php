<?php

try{
  $dbUserName = 'tweetadmin';
  $dbPassword = 'verystrongadminpassword'; // root | admin
  $dbConnection = 'mysql:host=db; dbname=tweeter; charset=utf8mb4'; 
  // utf8 every character in the world
  // utf8mb4 every character and also emojies
  $options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, // try-catch
    //PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_NUM 
    //PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC 
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ 
  ];
  $db = new PDO(  $dbConnection, 
                  $dbUserName, 
                  $dbPassword , 
                  $options );
  
}catch(PDOException $ex){
  http_response_code(500);
  header('Content-Type: aplication/json');
 // echo $ex;
  echo '{"message": "System error'.__LINE__.'"}';
  exit();
}














