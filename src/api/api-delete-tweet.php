<?php

//session engine
session_start();
if( ! isset($_SESSION['name']) ){
  header('Location: /../login.php');
}
 
try{

if (!isset($_GET['id'])) {
    http_response_code(400);
    header('Content-Type: application/json');
    echo '{"error":"missing id"}';
    exit();
}
 
if (strlen($_GET['id']) != 13) {
    http_response_code(400);
    header('Content-Type: application/json');
    echo '{"error":"id is not valid"}';
    exit();
}
 

require_once(__DIR__.'/../private/db.php');
  $q = $db->prepare(
      'SELECT * FROM tweets'
  );
  $q->execute();
  $aTweets = $q->fetchAll();

//   print_r($aTweets);

for ($i = 0; $i < count($aTweets); $i++) {
    if ($_GET['id'] == $aTweets[$i]->tweetId && $_GET['userId'] == $aTweets[$i]->tweetUserFk) {

        require_once(__DIR__.'/../private/db.php');
        $q = $db->prepare(
        'DELETE FROM `tweets` WHERE `tweets`.`tweetId` = :id'
    );
        $q->bindValue('id',$_GET['id']);
        $q->execute();

        exit();
    } 
} 
 
header('Content-Type: application/json');
http_response_code(400);
echo '{"message" :"tweet not found"}';
}

catch (Exception $ex) {
    http_response_code(500);
    header('Content-Type: application/json');
    echo '{"message":"error ' . __LINE__ . '"}';
   }
