<?php
// Prevent empty
if(! $_POST){ header('Location: signup.html'); }


// Get json data
try {
  require_once(__DIR__.'/../private/db.php');
  $q = $db->prepare(
      'SELECT * FROM users'
  );
  $q->execute();
  $jUsers = $q->fetchAll();
}
catch(PDOException $ex){
  sendError(500, 'system under maintainance', __LINE__);
}

function sendError($iErrorCode, $sMessage, $iLine){
  http_response_code($iErrorCode);
  echo '{"message":"'.$sMessage.'", "error":"'.$iLine.'"}';
  exit();
}


// Check for duplicates
foreach($jUsers as $jUser){
  if($_POST['userName'] == $jUser->userName){
    echo 'choose different name';
    exit();
  }
}

// Validate email
if(! filter_var($_POST['userEmail'], FILTER_VALIDATE_EMAIL) ){
  echo 'uncorect email';
  exit();
}

foreach($jUsers as $jUser){
  if($_POST['userEmail'] == $jUser->userEmail){
    echo 'you already have an account';
    exit();
  }
}

// Check password format
if(! $_POST['userPassword'] == preg_match('/(?=.*\d)(?=.*[A-Za-z]).{3,}/', $_POST['userPassword'])){
  echo 'Password need to contain at least 3 characters 1 letter and 1 digit';
  exit();
} 

// Hash password
  $passwordHash = password_hash($_POST['userPassword'], PASSWORD_DEFAULT);

// Ad user to database

  $jUser                 = new stdClass(); // {}
  $jUser->id             = uniqid();
  $jUser->name           = $_POST['userName'];
  $jUser->email          = $_POST['userEmail'];
  $jUser->password       = $passwordHash;
  $jUser->profileIMG     = 'img/default_profile_reasonably_small.png';
  $jUser->backgroundIMG  = 'img/default_img.jpg';
  

  try {
    require_once(__DIR__.'/../private/db.php');
    $q = $db->prepare(
        'INSERT INTO `users` VALUES
        (:id, :name, :email, :password, :userImage, :backgroundImage);'
    );
    $q->bindValue('id',$jUser->id);
    $q->bindValue('name',$jUser->name);
    $q->bindValue('email',$jUser->email);
    $q->bindValue('password',$jUser->password);
    $q->bindValue('userImage',$jUser->profileIMG);
    $q->bindValue('backgroundImage',$jUser->backgroundIMG);
    $q->execute();
}
catch(PDOException $ex){ 
  sendError(500, 'system under maintainance', __LINE__);
 
  }
 

  // array_push($jUsers, $jUser);
  // $sUsers = json_encode($jUsers, JSON_PRETTY_PRINT);
  // file_put_contents('users.json', $sUsers);

// Send user to login
  header('Location: /../login.php');
  exit();
