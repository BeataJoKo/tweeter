<?php

$id = $_GET['id'];
  
    


  try {
    require_once(__DIR__.'/../private/db.php');
    $q = $db->prepare(
        'SELECT `tweetId` AS "id", users.userProfileIMG AS "profileIMG", users.userName AS "name",`tweetText` AS "text",
        `tweetComments` AS "comments", `tweetShares` AS "shares", `tweetLoves` AS "loves", users.userId
        FROM tweets
        INNER JOIN users
        ON `tweetUserFk` = users.userId
        WHERE users.userId = :id'
    );
    $q->bindValue('id',$id);
    $q->execute();
    $aTweets = $q->fetchAll();
    echo json_encode($aTweets);
}
catch(PDOException $ex){
    sendError(500, 'system under maintainance', __LINE__);
  }
 
  function sendError($iErrorCode, $sMessage, $iLine){
    http_response_code($iErrorCode);
    header('Content-Type: application/json');
    echo '{"message":"'.$sMessage.'", "error":"'.$iLine.'"}';
    exit();
  }
?>
