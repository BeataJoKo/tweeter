<?php

//session engine
session_start();
if( ! isset($_SESSION['name']) ){
  header('Location: /../login.php');
}

try {
 
 if (!isset($_GET['id'])) {
 http_response_code(400);
 header('Content-Type: application/json');
 echo '{"error":"missing id"}';
 exit();
 }
 
 if (strlen($_GET['id']) != 13) {
 http_response_code(400);
 header('Content-Type: application/json');
 echo '{"error":"id is not valid"}';
 exit();
 }
 if (!isset($_POST['newText'])) {
 http_response_code(400);
 header('Content-Type: application/json');
 echo '{"error":"missing text"}';
 exit();
 }
 
 if (strlen($_POST['newText']) < 1) {
 http_response_code(400);
 header('Content-Type: application/json');
 echo '{"error":"tweet must be at least 1 characters"}';
 exit();
 }
 if (strlen($_POST['newText']) > 280) {
 http_response_code(400);
 header('Content-Type: application/json');
 echo '{"error":"tweet cannot be longer than 280 characters"}';
 exit();
 }
 
 // connect to the db
 require_once(__DIR__.'/../private/db.php');
  $q = $db->prepare(
      'SELECT * FROM tweets'
  );
  $q->execute();
  $aTweets = $q->fetchAll();
  // print_r($aTweets);


function sendError($iErrorCode, $sMessage, $iLine){
  http_response_code($iErrorCode);
  header('Content-Type: application/json');
  echo '{"message":"'.$sMessage.'", "error":"'.$iLine.'"}';
  exit();
} 



 for ($i = 0; $i < count($aTweets); $i++) {
  if ($_GET['id'] == $aTweets[$i]->tweetId and $_GET['userId'] == $aTweets[$i]->tweetUserFk) {

    require_once(__DIR__.'/../private/db.php');
    $q = $db->prepare(
        'UPDATE `tweets` SET `tweetText` = :newText WHERE `tweets`.`tweetId` = :id;'
    );
    $q->bindValue('id',$_GET['id']);
    $q->bindValue('newText',$_POST['newText']);
    $q->execute();


  echo '{"message":"tweet has been updated"}';
  
  exit();
 }
 }
 header('Content-Type: application/json');
 http_response_code(400);
 echo '{"message" :"tweet not found"}';
} 
 catch (Exception $ex) {
 http_response_code(500);
 header('Content-Type: application/json');
 echo '{"message":"error ' . __LINE__ . '"}';
}
