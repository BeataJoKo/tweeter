<?php
use PHPUnit\Framework\TestCase;

class DBTest extends TestCase
{
    static private $pdo = null;

    private $conn = null;

    public function setUp(): void
    {
        $this->pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    // public function tearDown(): void
    // {
    //     if (!$this->pdo)
    //         return;
    //     $this->pdo->query("DROP TABLE hello");
    // }


    public function testDate(): void
    {
        $sql = 'SELECT CURDATE()';
        $stuff = $this->pdo->query($sql)->fetch(PDO::FETCH_ASSOC);
        print($stuff[0]);
        $this->assertEquals(date("Y-m-d"),$stuff[0]);
    }
}


// SELECT CAST( GETDATE() AS Date ) ; 2019-08-17

// date("Y-m-d");

