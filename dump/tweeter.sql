-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2021 at 05:00 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tweeter`
--

-- --------------------------------------------------------

--
-- Table structure for table `tweets`
--

CREATE TABLE `tweets` (
  `tweetId` varchar(13) NOT NULL,
  `tweetUserFk` varchar(13) NOT NULL,
  `tweetText` varchar(280) NOT NULL,
  `tweetImage` varchar(50) DEFAULT NULL,
  `tweetLink` varchar(2000) DEFAULT NULL,
  `tweetComments` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `tweetShares` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `tweetLoves` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `tweetCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `tweetActive` tinyint(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tweets`
--

INSERT INTO `tweets` (`tweetId`, `tweetUserFk`, `tweetText`, `tweetImage`, `tweetLink`, `tweetComments`, `tweetShares`, `tweetLoves`, `tweetCreated`, `tweetActive`) VALUES
('1111111111111', '1111111111111', 'Tweet A', NULL, NULL, 0, 0, 0, '2021-04-29 16:51:20', 1),
('5f5d018eb2a41', '1111111111111', 'AAAAAAAAAAAAAAAAAAAA', NULL, NULL, 0, 0, 0, '2021-04-29 16:51:20', 1),
('5f5d5194ba79c', '5f5d1814f3b0d', 'AAAAAAAAAAAAAAAAAAAA', NULL, NULL, 0, 0, 0, '2021-04-29 16:54:33', 1),
('5f5d52072e4f1', '5f5d1814f3b0d', 'AAAAAAAAAAAAAAAAAAAA', NULL, NULL, 0, 0, 0, '2021-04-29 16:55:52', 1),
('5f5d52c1a33b5', '5f5d1814f3b0d', '...............', NULL, NULL, 0, 0, 0, '2021-04-29 16:55:52', 1),
('5f5ddd4618120', '5f5bc2f89c80a', 'bollllllllaaa', NULL, NULL, 0, 0, 0, '2021-04-29 16:53:03', 1),
('5f5df8fc0f000', '5f5bc2f89c80a', 'What a story!!!!!!!!!!!!!!!!!!!!!!!!!!!!', NULL, NULL, 0, 0, 0, '2021-04-29 16:53:03', 1),
('5f5dfaa05ff0c', '5f5bc2f89c80a', '###### <3', NULL, NULL, 0, 0, 0, '2021-04-29 16:54:33', 1),
('5f5eb6d346fc6', '5f5eafaee767c', 'kjkjkjkjkjk', NULL, NULL, 0, 0, 0, '2021-04-29 16:57:17', 1),
('5f608c4acd978', '5f5bc2f89c80a', 'Plum plum plum plum', NULL, NULL, 0, 0, 0, '2021-04-29 16:57:17', 1),
('5f99e4534ea0c', '5f5bc2f89c80a', 'jhjhjhjpopopopop', NULL, NULL, 0, 0, 0, '2021-04-29 16:58:37', 1),
('5fd01483a91f6', '5f5bc2f89c80a', 'ZZZZ', NULL, NULL, 0, 0, 0, '2021-04-29 16:58:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` varchar(13) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `userEmail` varchar(20) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `userProfileIMG` varchar(50) NOT NULL DEFAULT 'img\\/default_profile_reasonably_small.png',
  `userBackgroundIMG` varchar(50) NOT NULL DEFAULT 'img\\/default_img.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `userName`, `userEmail`, `userPassword`, `userProfileIMG`, `userBackgroundIMG`) VALUES
('1111111111111', 'DR Nyheder', 'a@a.com', 'kubek36iko', 'img\\/dr.jpg', 'img\\/WC2Kr7PO.jpg'),
('5f5bc2f89c80a', 'New York Times Arts', 'c@c.com', '$2y$10$hyKSY8yuXwZfkuKTrHU1v.IkQ3\\/\\/1gTxlYJrsFTL0kZdqWwE17c8.', 'img\\/times.jpg', 'img\\/de8hJ4X8.jpg'),
('5f5d1814f3b0d', 'Simon Someone', 'x@x.com', '$2y$10$uYJFhdQ9DYqq9ZVr5qARVuH03xCLOFfuyPXu5p84e7BlUV1J6VDf.', 'img\\/icon.jpg', 'img\\/RPfkf_Ia.jpg'),
('5f5eafaee767c', 'xxklkl', 'c@d.com', '$2y$10$kZCYT318U\\/jrhBBDMqJf4.5lfK3sy\\/deOdaMBrdMakKiuJMwN4Mri', 'img/default_profile_reasonably_small.png', 'img\\/default_img.jpg'),
('5f5fab2711216', 'xx', 'z@z.com', '$2y$10$FNE9rZGWTjEiyRssAEiTlu56rgv6Ha4ai.vrFwj7pBiRXnVam19ly', 'img\\/default_profile_reasonably_small.png', 'img\\/default_img.jpg'),
('5f60b67e47586', 'abc', 'zccc@z.com', '$2y$10$fF0u2oOQFFvxb\\/N5QeQtBu6vyY3Kdfn\\/YL4T\\/4FAOWEgwmpzn1kW2', 'img\\/default_profile_reasonably_small.png', 'img\\/default_img.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tweets`
--
ALTER TABLE `tweets`
  ADD PRIMARY KEY (`tweetId`),
  ADD UNIQUE KEY `tweetId` (`tweetId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `userName` (`userName`),
  ADD UNIQUE KEY `userEmail` (`userEmail`),
  ADD UNIQUE KEY `userId` (`userId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
